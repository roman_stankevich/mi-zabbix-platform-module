# README #

CloudFormation Zabbix Templates for Platform Deployment. 

Contains two parts:

* Head Zabbix Server (2 web nodes, 1 server node plus RDS MySQL)

* Zabbix Client [Node](https://www.zabbix.com/documentation/2.2/manual/distributed_monitoring/nodes) (2 web nodes, 1 server node plus RDS MySQL)

Jira tasks:

* [ISMG-5](https://imagination.jira.com/browse/ISMG-5)
* [ISMG-6](https://imagination.jira.com/browse/ISMG-6)

### What was added/changed as additional features ###

* DHCP Options Set with appropriate DNS servers in a Hosted Zone
* In Route 53: CNAME for LB for WebServers plus A record for Zabbix backend

### Input parameters (mandatory parameters) ###

###For Both Stacks:###
* Hosted Zone ID

###For the Head Zabbix Stack:###
* Nameservers separated by comma, e.g.: 205.251.193.75,AmazonProvidedDNS,205.251.195.145

*Note: AmazonProvidedDNS shouldn't be the first*

###For the Zabbix Node Stack:###
* VPC ID
* Subnet A ID
* Subnet C ID
* Security Group ID
* Database Node ID
* Client Name

### NOTE ###
You should include the task in Ansible for deletion of Route 53 records first of all before the deletion of a stack.